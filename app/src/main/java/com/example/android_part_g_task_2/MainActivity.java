package com.example.android_part_g_task_2;

/*

2. Используя API сервиса https://restcountries.eu/ создать приложение с одним экраном.
На экране должна быть возможность ввода кода страны (например UA или US).
И по нажатию на кнопку программа должна отобразить инфу о стране (2-3 пункта).

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android_part_g_task_2.model.RequestModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.am_country_code)
    EditText countryCode;

    @BindView(R.id.am_country_info)
    TextView countryInfo;

    @BindView(R.id.am_get_country_info)
    Button getCountryInfo;

    Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        getCountryInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getInfo();
            }
        });

//        disposable = ApiService.getData()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeOn(Schedulers.io())
//                .subscribe(new Consumer<List<RequestModel>>() {
//                    @Override
//                    public void accept(List<RequestModel> requestModel) throws Exception {
//                        Toast.makeText(MainActivity.this, requestModel.get(0).name, Toast.LENGTH_SHORT).show();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(Throwable throwable) throws Exception {
//                        Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
//                    }
//                });
    }

    public void getInfo() {
        disposable = ApiService.getCountryByCode(countryCode.getText().toString().trim())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<RequestModel>() {
                    @Override
                    public void accept(RequestModel requestModel) throws Exception {

                        StringBuilder countryInfoBuilder = new StringBuilder();

                        countryInfoBuilder.append("Name: " + requestModel.getName() + "\n");
                        countryInfoBuilder.append("Region: " + requestModel.getRegion() + "\n");
                        countryInfoBuilder.append("Capital: " + requestModel.getCapital() + "\n");
                        countryInfoBuilder.append("Population: " + requestModel.getPopulation());

                        countryInfo.setText(countryInfoBuilder.toString());
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (disposable != null && disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}



